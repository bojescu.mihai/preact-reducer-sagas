import { Reducer, useCallback, useReducer } from 'preact/hooks'
import { ReducerSaga } from '../types'

export const useReducerWithSaga = <State, Actions>(
  reducer: Reducer<State, Actions>,
  initialState: State,
  sagas: ReducerSaga<State, Actions, Actions>
): [State, (action: Actions) => void] => {
  const [state, dispatch] = useReducer<State, Actions>(reducer, initialState)

  const sagaInterceptor = useCallback((action: Actions) => {
    sagas({ action, state, dispatch })
    dispatch(action)
  }, [sagas, state])

  return [state, sagaInterceptor]
}
