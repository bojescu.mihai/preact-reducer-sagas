
import { ReducerSaga } from '../../../../lib'
import * as number from '../number'
import * as uuid from '../uuid'
import { Actions, State } from './types'

export const saga: ReducerSaga<State, Actions, Actions> = ({ state: { Number, UUID }, action, dispatch }) => {
  number.saga({ state: Number, action, dispatch })
  uuid.saga({ state: UUID, action, dispatch })
}
