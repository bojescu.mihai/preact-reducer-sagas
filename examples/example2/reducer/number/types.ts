export type State = {
  data: null | number,
  isLoaded: boolean,
  error: null | Error
}

export type Actions =
  | { type: 'FETCH' }
  | { type: 'FETCH_SUCCESSFUL', payload: number }
  | { type: 'FETCH_FAILED', payload: Error }
